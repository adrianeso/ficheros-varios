
Master Services Agreement

This Agreement was last updated on Octubre 12, 2021.

This Master Services Agreement (“Agreement”) governs the access and use of Udemy Business and Udemy Government.

    Definitions. As used in this Agreement, the following terms have the meaning set forth below.

        “Affiliate” means any entity that directly or indirectly controls, is controlled by, or is under common control with the subject entity.

        "The “CorpU Offering” means an online education product that includes cohort based, synchronous and asynchronous, learning programs with certain Courses accessible through a social learning online platform that include learning services.

        “Customer” means the party entering into an Order Form with Udemy, or otherwise signing up to use the Services.

        “Courses” means the online courses provided by Udemy as part of the Services.

        The “Fees” means the amounts payable by Customer to Udemy for access to the Services.

        “Order Form” means the ordering document mutually executed by Udemy and Customer, including electronic orders submitted by Customers purchasing the Team Plan version of the Services, specifying: (1) the Fees payable by Customer to Udemy for provision of the Services, (2) the duration of the Services to be provided by Udemy to Customer, (3) the number of Users authorized to access the Services, (4) billing and payment information regarding Udemy’s provision of the Services to Customer, and (5) any other applicable quantity specifications regarding Customer’s purchase of the Services.

        “Personal Data” means any personal data that Customer submits into the Services.

        The “Services” means any of the following offerings that may be purchased by Customer from Udemy: (1) Udemy Business or Udemy Government, a platform for online education provided by Udemy through individual, asynchronous learning, that includes the Courses, (2) the CorpU Offering, and 3) Udemy Business Pro.

        “Udemy” means Udemy, Inc., or one of its Affiliates.

        “Udemy Business Pro” means an interactive cloud workspace containing hands-on learning labs.

        “Users” means the employees and contractors that Customer authorizes to access and use the Services.

    Provision of the Services. Udemy agrees to make the Services available to Customer, its Affiliates, and its Users pursuant to the terms of this Agreement, and as specified in an Order Form. Where an Affiliate enters into its own separate Order Form governed by this Agreement, then for purposes of that Order Form, the Affiliate shall be considered “Customer”. In the event Customer is purchasing a subscription to the “Team Plan” version of the Services, then Customer acknowledges and agrees that certain features generally available in the Services may not be available to Customer. If Customer is purchasing access to the CorpU Offering, then the additional, product specific, terms and conditions available here will apply. If Customer is purchasing access to Udemy Business Pro, then the additional, product specific, terms and conditions available here will apply.

    Restrictions.

        Customer shall not, nor shall it permit its Users to:

            Copy, distribute, create derivative works, hack, modify, or interfere with, the proper working of the Services, any of the Courses, or any third-party system made available through the Services,
            Input any inappropriate, infringing, offensive, racist, hateful, sexist, pornographic, harassing, defamatory or libelous content into the Services or instruct Udemy to include any such content in the Services,
            Scrape, spider, or utilize other automated means of any kind to access the Services, including but not limited to accessing API endpoints for which Customer or its Users have not been provided authorization by Udemy,
            Use the Services in order to build a competitive product to the Services,
            Share login access to the Services among multiple individuals, transfer a User license (except in connection with a termination of employment), or otherwise permit any party other than the Users to use the Services,
            Introduce any computer code, file, or program that may damage the Services,
            Use the Services in any manner that is unlawful or that infringes the rights of others, or
            Use Udemy’s APIs with any third party without prior written approval (certain third parties currently have pre-approval as provided within Udemy’s API documentation).
            Use the Services to transmit any unsolicited commercial communications,
            Use the Services for any purpose other than internal learning, or
            Permit any individual that is under the age of 13 years old to use the Services.

        Customer represents and warrants that neither it nor its Users are (a) located in, or a resident of, any country that is subject to applicable U.S. trade sanctions or embargoes (such as Cuba, Iran, North Korea, Sudan, or Syria), or (b) a person or entity who is named on any U.S. government specially designated national or denied-party list. Customer shall not permit any User to access or use the Services in a U.S. embargoed country or in violation of any U.S. export law or regulation

    Violations of Restrictions. In the event that Udemy determines that Customer or any of its Users has violated the restrictions set forth in Section 3 above, Udemy may notify Customer of such violation and allow customer a 10 day cure period to remedy such violation. If Customer fails to cure such remediable violation, then Udemy may terminate or suspend access to the Services for Customer or the relevant Users. Irrespective of the cure period stated above, Udemy reserves the right in its sole discretion to terminate or suspend access to the Services for Customer or the relevant Users, at any time, if immediate action is required to address imminent potential harm or damages.

    Fees. Customer will pay the Fees as set forth in one or more Order Forms. Unless stated otherwise in an Order Form, all fees are payable in US dollars. Any future incremental add-on or renewal orders after the initial subscription period (as set forth in an Order Form) shall be subject to the subscription standard price in effect at time of purchase. In the event that Customer is late in making payments, then Udemy reserves the right to charge the greater of 1.5% interest per month or the maximum interest permitted by law, and Customer will be liable for all third-party collection costs.

    Taxes. The Fees and other amounts required to be paid hereunder do not include any amount for taxes, including any applicable sales, use, excise, or other transaction-based tax ("Taxes") or levy (including interest and penalties). Customer agrees to pay all amounts payable under this Agreement free and clear of all deductions or withholdings or rights of counter claim or set-off, unless required by law. If a deduction or withholding is so required, then Customer agrees to pay such additional amount as to ensure that the net amount received and retained by Udemy equals the full amount that Udemy would have received had the deduction or withholding not been required. Customer shall reimburse Udemy and hold Udemy harmless for Taxes or levies to which Udemy is required to collect or remit to applicable tax authorities. This provision does not apply to Udemy's income, franchise and employment taxes or any taxes for which Customer is exempt provided Customer has furnished Udemy with a valid tax exemption certificate. To the extent a taxing authority changes their position or taxing policy requiring Udemy to collect a Tax or levy from Customer, Udemy will add the Tax or levy to the Customer invoice.

    Confidentiality.

        Scope of Confidentiality. Each party agrees that all code, inventions, know-how, or business, technical, and financial information disclosed to such party (“Receiving Party”) by the disclosing party (“Disclosing Party”), constitute the confidential information of the Disclosing Party (“Confidential Information”), provided that it is either identified as confidential at the time of disclosure, or should be reasonably known by the Receiving Party to be confidential due to the nature of the information disclosed. Personal Data is considered Confidential Information. Confidential Information will not, however, include any information that: (1) was publicly known and made generally available in the public domain prior to the time of disclosure by the Disclosing Party, (2) becomes publicly known and made generally available after disclosure by the Disclosing Party to the Receiving Party through no action or inaction of the Receiving Party, (3) is already in the possession of the Receiving Party at the time of disclosure by the Discloser, (4) is obtained by the Receiving Party from a third party without a known breach of the third party’s obligations of confidentiality, or (5) is independently developed by the Receiving Party without use of or reference to the Confidential Information. The Receiving Party may disclose the Disclosing Party’s Confidential Information if required by law so long as the Receiving Party gives the Disclosing Party prompt written notice of the requirement prior to the disclosure and assistance in obtaining an order protecting the information from public disclosure.

        Non-Use and Non-Disclosure. Except as expressly authorized herein or as necessary to perform its obligations hereunder, the Receiving Party agrees to: (1) not disclose any Confidential Information to third parties, and (2) not use Confidential Information for any purpose other than as necessary to exercise its rights or perform its obligations hereunder.

        Processing of Personal Data. Notwithstanding the provisions of this section, Customer agrees that Udemy may process Personal Data as necessary for: (1) storage and processing in accordance with the Agreement and applicable Order Form(s); (2) processing initiated by Users in their use of the Services; and (3) processing to comply with other documented reasonable instructions provided by User (e.g. via email or support tickets) where such instructions are consistent with the terms of the Agreement. To the extent that Customer is subject to a local data privacy law (including but not limited to the General Data Protection Regulation or the California Consumer Privacy Act), then Customer agrees to request from Udemy a data protection agreement prior to providing any Personal Data to Udemy.

    Term and Termination.

        Duration of Term. This Agreement will commence on the Effective Date, and will continue until all Order Forms hereunder have expired or have been terminated. The duration of the Services will be specified in each applicable Order Form. Unless otherwise specified in an applicable Order Form, and with the exception of Customers on the Udemy Business Team plan that have disabled auto-renewal within the Services, Order Forms will renew automatically, unless terminated by either party by giving at least 30 days written notice prior to the end of the then-current term.

        Termination for Material Breach. Either party may terminate this Agreement and any applicable Order Forms in the event that the other party materially breaches this Agreement, by providing 30 days written notice, unless such breach is cured during such 30 day notice period. In the event that Customer terminates this Agreement or any Order Form due to material breach by Udemy, then Customer will be entitled to receive a pro-rated refund for Services not rendered past the termination date. Sections 5-11 and 15-18, as well as any accrued rights to payment, will survive any termination or expiration of the Agreement.

    WARRANTY DISCLAIMER. EXCEPT AS OTHERWISE AGREED UPON BY THE PARTIES, UDEMY PROVIDES THE SERVICES AS-IS AND DISCLAIMS ALL WARRANTIES RELATING TO THE SERVICES AND ANY THIRD PARTY SYSTEMS OR PLATFORMS ACCESSIBLE THROUGH THE SERVICES, EXPRESS, OR IMPLIED, INCLUDING BUT NOT LIMITED TO, ANY WARRANTIES RELATING TO MERCHANTABILITY, ACCURACY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, OR AVAILABILITY.

    Limitation of Liabilities.

        NEITHER PARTY WILL BE LIABLE WITH RESPECT TO ANY SUBJECT MATTER OF THIS AGREEMENT OR RELATED TERMS AND CONDITIONS UNDER ANY THEORY OF CONTRACT, NEGLIGENCE, STRICT LIABILITY, OR OTHER THEORY FOR: (1) ANY INDIRECT, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR (2) ANY AMOUNTS IN EXCESS OF THE FEES PAID OR PAYABLE BY CUSTOMER TO UDEMY IN THE 12 MONTHS PRIOR TO THE DATE THE RELEVANT CLAIM AROSE.

        Exceptions to Limitation of Liability. Notwithstanding the limitation of liability set forth above: (1) Customer’s liability arising from its violation of Section 3 of this Agreement, and either party’s liability arising from the indemnification provisions of this Agreement, will not be limited, and (2) Each party’s liability arising from breach of its confidentiality obligations hereunder will be limited to three times the amount of Fees paid or payable by Customer to Udemy in the 12 months prior to the date a claim arose.

    Indemnification.

        Udemy's Indemnification Obligations. Udemy agrees to defend Customer for all third party claims arising from an allegation that Customer’s use of the Services as permitted under this Agreement infringes upon a third party’s intellectual property rights (“Claim Against Customer”), and indemnify Customer from any damages, reasonable attorney fees, and costs incurred by Customer as a result of a Claim Against Customer. In the event that the Services become subject to a third-party intellectual property claim, or Udemy believes that the Services will become subject to such a claim, then Udemy may elect to: (1) modify the Services so that they are no longer allegedly infringing, (2) obtain a license for Customer’s continued use of the Services, or (3) terminate this Agreement or any applicable Order Forms, and provide Customer a pro-rated refund for Services not rendered past the termination date. This section states Udemy’s sole liability to the Customer with respect to a claim that any part of the Services infringes the intellectual property rights of a third party.

        Customer's Indemnification Obligations. Customer agrees to defend Udemy for all third-party claims arising from Customer’s violations of Sections 3(a) i. and ii., and 3(b) of this Agreement (“Claim Against Udemy”), and indemnify Udemy from any damages, reasonable attorney fees, and costs incurred by Udemy as a result of a Claim Against Udemy.

        Requirements for Indemnification. In order for the indemnification obligations hereunder to apply, the party seeking indemnification must: (1) promptly tender a claim for indemnification, (2) allow the indemnifying party sole control of the defense or settlement of the underlying claim, and (3) reasonably assist with any defense or settlement of the underlying claim at the indemnifying party’s request and expense.

    Anti-Corruption. Neither party has received or been offered any illegal or improper bribe, kickback, payment, gift, or thing of value from an employee or agent of the other party in connection with this Agreement. Reasonable gifts and entertainment provided in the ordinary course of business do not violate the above restriction. Upon learning of any violation of this restriction, Customer agrees to promptly notify Udemy’s legal department, by emailing legal@udemy.com.

    Publicity. Customer grants Udemy the right to use Customer’s company name and logo as a reference for marketing or promotional purposes on Udemy’s website and in other promotional materials.

    Force Majeure. Neither party will be liable for any failure or delay in the performance of its obligations hereunder to the extent caused by a condition that is beyond a party’s reasonable control, including but not limited to natural disaster, civil disturbance, acts of terrorism or war, labor conditions, failure by a third party hosting provider or utility provider, governmental actions, interruption or failure of the Internet or any utility service, or denial of service attacks.

    Severability. If any provision of this Agreement is held by a court of competent jurisdiction to be contrary to law, the provision shall be deemed null and void, and the remaining provisions of this Agreement shall remain in effect.

    Governing Law Venue, and Attorney’s Fees. This Agreement and any disputes arising under it will be governed by the laws of the State of California without regard to its conflict of laws provisions, and each party consents to the personal jurisdiction and venue of the state or federal courts located in San Francisco, California. In the event of any dispute between the parties regarding the terms of this Agreement, the party prevailing in such dispute shall be entitled to collect from the other party all costs incurred in such dispute, including reasonable attorneys’ fees.

    Entire Agreement. This Agreement constitutes the entire agreement between the parties pertaining to the subject matter hereof and supersedes all prior or contemporaneous oral or written communications, proposals, and representations with respect to its subject matter. This Agreement and any mutually executed Order Forms shall apply in lieu of the terms or conditions in any purchase order or other documentation that Customer provides, and all such terms and conditions are null and void and superseded by this Agreement and any mutually executed Order Forms. This Agreement, or any part thereof, may be modified by Udemy at any time, including the addition or deletion of terms at any time, and such modifications, additions or deletions will be effective immediately upon posting.

    Contracting Party, Governing Law, and Currency for Indian Customers. As of June 1, 2020, if Customer is located in India, then Customer is contracting with Udemy India LLP under this Agreement. In such case, notwithstanding Section 17 above, this Agreement and any disputes arising under it will be governed by the laws of India, and both parties consent to the exclusive jurisdiction and venue of courts in Delhi, India for all disputes arising out of this Agreement. In addition, if Customer is located in India, notwithstanding Section 17 above, then any dispute, claim, or any non-payment (any of which shall be treated as a dispute) whether present or future, whatsoever between the parties under, arising out of, relating to or in connection with this Agreement shall be settled by mandatory arbitration in accordance with the provisions of the Arbitration and Conciliation Act, 1996 by a sole arbitrator mutually appointed by the parties and both parties consent to such mandatory arbitration. Either party may serve the other party with a notice in writing specifying the existence and nature of the dispute and the intention to refer the dispute to arbitration. If the parties are unable to agree on a sole arbitrator within 30 days of such notice, each Party shall appoint an arbitrator, and the arbitrators so appointed shall jointly appoint the third arbitrator. The award determined through arbitration shall be final and binding. The venue of such arbitration shall be in Delhi. The proceedings shall be conducted in English. Notwithstanding Section 5 above, if Customer is located in India, then all fees payable by Customer will be in Indian Rupees.

