
Condiciones del instructor

Estas Condiciones del instructor se actualizaron por última vez el Mayo 3, 2021.

Al registrarse para convertirse en instructor en la plataforma de Udemy, acepta cumplir estas Condiciones del instructor ("Condiciones"). Estas Condiciones tratan detalles de los aspectos de la plataforma de Udemy que resultan relevantes para los instructores y se incorporan mediante referencias en nuestras Condiciones de uso, las condiciones generales que rigen su uso de nuestros Servicios. Los términos en mayúsculas que no se definan en estas Condiciones se definen según lo especificado en las Condiciones de uso.

Como instructor, usted efectúa la contratación directamente con Udemy, Inc. (una sociedad de Delaware en los Estados Unidos de América), independientemente de que otra filial de Udemy le facilite los pagos.
1. Obligaciones del instructor

Como instructor, es el responsable de todo el contenido que publica, incluidas las clases, los cuestionarios, los ejercicios de codificación, los exámenes de prueba, las tareas, los recursos, las respuestas, el contenido de la página de inicio del curso, los laboratorios, las evaluaciones y los anuncios ("Contenido remitido").

Usted declara y garantiza que:

    proporcionará y mantendrá información precisa sobre su cuenta;
    posee o tiene las licencias, los derechos, los consentimientos, los permisos y la autoridad necesarios para autorizar a Udemy para que utilice su Contenido remitido según lo que se especifica en estas Condiciones y en las Condiciones de uso;
    su Contenido remitido no infringe ni malversa ningún derecho de propiedad intelectual de un tercero;
    tiene las calificaciones, credenciales y experiencia requeridas (incluida la educación, la formación, los conocimientos y las habilidades) para enseñar y ofrecer los servicios que ofrece mediante su Contenido remitido y su uso de los Servicios; y
    garantizará una calidad de servicio acorde con los estándares de su sector y los servicios de instrucción en general.

Usted garantiza que no:

    publicará ni proporcionará ningún contenido o información inadecuado, ofensivo, racista, lleno de odio, sexista, pornográfico, falso, engañoso, incorrecto, ilegítimo, difamatorio o calumnioso;
    publicará ni transmitirá publicidad no solicitada o no autorizada, materiales promocionales, correo basura, spam ni cualquier otra forma de solicitación (comercial o de otro tipo) a través de los Servicios o a cualquier usuario;
    utilizará los Servicios para nada que no sea proporcionar servicios de tutoría, enseñanza e instrucción a los estudiantes;
    participará en ninguna actividad que nos exigiría obtener licencias o pagar derechos a terceros, incluida la necesidad de pagar derechos por la ejecución pública de una obra musical o grabación de sonido;
    enmarcará ni integrará los Servicios (como para integrar una versión gratuita de un curso) ni los eludirá de ninguna otra forma;
    se hará pasar por otra persona ni obtendrá acceso sin autorización a la cuenta de otra persona;
    interferirá ni evitará de ninguna otra forma que otros instructores presten sus servicios o contenidos; ni
    abusará de los recursos de Udemy, incluidos los servicios de asistencia.

2. Licencia para Udemy

Otorga a Udemy los derechos que se detallan en las Condiciones de uso para ofrecer, comercializar y explotar de cualquier otra forma su Contenido remitido. Esto incluye el derecho de añadir subtítulos o modificar de cualquier otra forma el Contenido remitido para garantizar la accesibilidad. También autoriza a Udemy a conceder estos derechos sobre el Contenido remitido a terceros mediante sublicencia, bien directamente a estudiantes, bien a través de terceros como distribuidores, revendedores, sitios afiliados, sitios de ofertas y publicidad de pago en plataformas de terceros.

A menos que se acuerde lo contrario (incluido en nuestra Política de promociones), tiene el derecho de eliminar todo su Contenido remitido o cualquier parte del mismo de los Servicios en cualquier momento. Salvo que se acuerde lo contrario, el derecho de Udemy de conceder licencias de los derechos de esta sección cesará con respecto a los nuevos usuarios 60 días después de que se retire el Contenido remitido. No obstante, (1) los derechos otorgados a los estudiantes antes de la retirada del Contenido remitido continuarán de conformidad con las condiciones de esas licencias (incluidas las concesiones de acceso de por vida) y (2) el derecho de Udemy de utilizar tal Contenido remitido para fines de marketing seguirá vigente tras la rescisión.

Podemos grabar todo su Contenido remitido o cualquier parte del mismo para controlar la calidad y entregar, comercializar, promocionar, hacer demostraciones o gestionar los Servicios. Usted concede permiso a Udemy para usar su nombre, retrato, voz o imagen para ofrecer, entregar, comercializar, promocionar, hacer demostraciones y vender los Servicios, su Contenido remitido o el contenido de Udemy, así como renuncia a cualquier derecho de privacidad, publicidad o cualquier otro derecho de naturaleza semejante, hasta donde lo permita la ley vigente.
3. Sección Confianza y Seguridad
3.1 Políticas de confianza y seguridad

Acepta cumplir las políticas de confianza y seguridad de Udemy, la política de temas restringidos y otros estándares o políticas de calidad del contenido dictados por Udemy cada cierto tiempo. Debe revisar estas políticas de forma periódica para asegurarse de que cumple con las actualizaciones que se les realicen. Entiende que su uso de los Servicios está sujeto a la aprobación de Udemy, que podemos conceder o denegar bajo nuestro criterio exclusivo.

Nos reservamos el derecho de eliminar contenidos, suspender pagos o vetar a instructores por cualquier motivo, en cualquier momento y sin previo aviso, incluido en los casos en los que:

    un instructor o un contenido no cumpla con nuestras políticas o condiciones legales (incluidas las Condiciones de uso);
    un contenido se encuentre por debajo de nuestros estándares de calidad o tenga un impacto negativo en la experiencia del estudiante;
    un instructor presente un comportamiento que pueda afectar de forma desfavorable a Udemy o derivar en su descrédito, menosprecio, escándalo o ridículo público;
    un instructor contrate los servicios de un comerciante u otro socio comercial que infrinja las políticas de Udemy;
    un instructor utilice los Servicios de algún modo que constituya competencia desleal, como la promoción de su negocio fuera del sitio de algún modo que infrinja las políticas de Udemy; o
    así lo determine Udemy bajo su criterio exclusivo.

3.2 Instructores colaboradores y profesores asistentes

La plataforma de Udemy le permite añadir a otros usuarios como instructores colaboradores o profesores asistentes para el Contenido remitido que administra; debe cumplir con nuestras Normas y directrices para las relaciones de colaboradores al emprender tales acciones. Al añadir un instructor colaborador o profesor asistente, entiende que los está autorizando a tomar determinadas medidas que afectan a su cuenta de Udemy y al Contenido remitido. Las infracciones de las condiciones y las políticas de Udemy por parte de sus instructores colaboradores o profesores asistentes también pueden afectar a su cuenta de Udemy y al Contenido remitido. Udemy no puede asesorar sobre ninguna pregunta ni mediar en ningún conflicto entre usted y tales usuarios. Si sus instructores colaboradores cuentan con un reparto de ingresos asignado, su parte se abonará a partir de su reparto de ingresos obtenidos en función de las proporciones que haya especificado en su configuración de Gestión del curso en la fecha de la compra.
3.3 Relación con otros usuarios

Los instructores no tienen una relación contractual directa con los estudiantes, por lo que la única información que recibirá sobre los estudiantes será la que se le proporcione a través de los Servicios. Acepta que no utilizará los datos que reciba para fines distintos a proporcionarle sus servicios a esos estudiantes en la plataforma de Udemy, así como que no solicitará datos personales adicionales ni almacenará los datos personales de los estudiantes fuera de la plataforma de Udemy. Usted acepta indemnizar a Udemy ante cualquier reclamación que surja de su uso de los datos personales de los estudiantes.
3.4 Esfuerzos contra la piratería

Nos asociamos con proveedores antipiratería para ayudar a proteger su contenido de un uso no autorizado. Para habilitar esta protección, por la presente designa a Udemy y a nuestros proveedores antipiratería como sus agentes para el fin de ejercer los derechos de autor de cada uno de sus contenidos, mediante procesos de aviso y eliminación (según la legislación de derechos de autor correspondiente, como el Acta de derechos de autor digitales del milenio [Digital Millennium Copyright Act]), así como para otros esfuerzos por hacer cumplir dichos derechos. Otorga a Udemy y a nuestros proveedores antipiratería la autoridad principal para enviar avisos en su nombre para defender sus intereses relacionados con los derechos de autor.

Acepta que Udemy y nuestros proveedores antipiratería mantendrán los derechos anteriores a menos que los revoque enviando un correo electrónico a piracy@udemy.com con el asunto "Revoke Anti-Piracy Protection Rights" desde la dirección de correo electrónico asociada a su cuenta. Cualquier revocación de derechos surtirá efecto 48 horas después de que la recibamos.
4. Precios
4.1 Configuración de precios

Al crear Contenido remitido disponible para la compra en Udemy, se le solicitará que seleccione un precio base ("Precio base") para el mismo de una lista de categorías de precio disponibles. Como alternativa, puede decidir ofrecer su Contenido remitido de forma gratuita. Como instructor premium, también se le ofrecerá la oportunidad de participar en determinados programas promocionales de conformidad con las condiciones de nuestra Política de promociones ("Programas promocionales").

Si decide no participar en ningún Programa promocional, catalogaremos su Contenido remitido en el Precio base o el equivalente local o de la aplicación móvil más cercano (según lo que se detalla a continuación). Si decide participar en un Programa promocional, es posible que establezcamos un precio con descuento o precio de catálogo diferente para su Contenido remitido, tal y como se describe en la Política de promociones.

Cuando un estudiante realiza una compra utilizando una moneda extranjera, convertimos el Precio base o el precio del Programa promocional correspondiente a la moneda pertinente del estudiante mediante una tasa de conversión de la moneda extranjera aplicable a todo el sistema. Esta tasa de conversión la establece Udemy y se fija periódicamente en una tabla de categorías de precios correspondientes por moneda ("Matriz de precios por nivel"). Como la Matriz de precios por nivel es fija, estas tasas de conversión puede que no sean idénticas a la tasa del mercado aplicable que esté vigente cuando se procese una transacción. Nos reservamos el derecho de actualizar la Matriz de precios por nivel en cualquier momento. Puede consultar la Matriz de precios por nivel e información adicional sobre los niveles de precios de Udemy aquí.

Cuando un estudiante realiza una compra a través de una aplicación móvil, será la matriz de precios del proveedor de la plataforma móvil la que tendrá el control, y elegiremos la categoría de precio más cercana al Precio base o al precio del Programa promocional aplicable. Como las plataformas móviles imponen sus propias tasas de conversión de las divisas, las conversiones de los precios de las aplicaciones móviles puede que no se correspondan con las conversiones de la Matriz de precios por nivel.

Usted nos concede el permiso de compartir su Contenido remitido de forma gratuita con nuestros empleados y socios selectos, así como en los casos en los que tengamos que restaurar cuentas de acceso que hayan adquirido su Contenido remitido anteriormente. Entiende que no recibirá ninguna compensación en estos casos.
4.2 Impuestos sobre las transacciones

Si un estudiante adquiere un producto o servicio en un país que requiere que Udemy abone impuestos sobre las ventas o el uso, impuestos sobre el valor añadido (IVA) u otros impuestos sobre las transacciones similares ("Impuestos sobre las transacciones") nacionales, estatales o locales, en virtud de las leyes aplicables recaudaremos y abonaremos esos Impuestos sobre las transacciones a las autoridades fiscales competentes en relación con tales ventas. Podemos aumentar el precio de venta según nuestro criterio si determinamos que se van a aplicar tales impuestos. En el caso de las compras realizadas a través de aplicaciones móviles, la plataforma móvil (como la App Store de Apple o Google Play) recauda los Impuestos sobre las transacciones aplicables.
4.3 Programas promocionales

Udemy ofrece varios programas de marketing opcionales (Programas promocionales) en los que puede elegir participar, tal y como se detalla en nuestra Política de promociones. Estos programas pueden ayudarle a aumentar su potencial de ingresos en Udemy al encontrar el precio ideal para su Contenido remitido y ofrecerlo a través de colecciones de suscripciones.

No hay ningún coste por adelantado por participar en estos programas y puede modificar su grado de participación en cualquier momento, aunque los cambios que realice no se aplicarán a las campañas activas actualmente y determinados programas pueden tener requisitos adicionales al finalizarse.
5. Pagos
5.1 Reparto de ingresos

Cuando un estudiante adquiere su Contenido remitido, calculamos el total bruto de la venta como el importe efectivamente percibido por Udemy del estudiante ("Total bruto"). De este total, descontamos los Impuestos sobre las transacciones, las tarifas de las plataformas móviles que se aplican a las ventas en dichos proveedores móviles, un 3 % de tasas servicio y procesamiento (excepto en Japón, donde descontamos un 4 % de tasas) en el caso de las ventas no realizadas a proveedores móviles y cualquier importe abonado a terceros en relación con los Programas promocionales para calcular el total neto de la venta ("Total neto").

Si no participa en ninguno de los Programas promocionales de Udemy, y a excepción de las ventas mediante códigos de cupón generados por el instructor o enlaces de referencia a cursos, tal y como se describe a continuación, su reparto de ingresos será el 37 % del Total neto menos las deducciones aplicables, como los reembolsos a los estudiantes. Si modificamos esta cuota de pago, le avisaremos con 30 días de antelación por algún medio visible, como por correo electrónico o publicando un aviso a través de nuestros Servicios.

Si participa en alguno de los Programas promocionales, el reparto de ingresos pertinente puede que sea diferente y será según lo especificado en la Política de promociones.

Udemy realiza todos los pagos de los instructores en dólares estadounidenses (USD), independientemente de la moneda en que se realizase la venta. Udemy no será responsable de las tarifas de conversión de moneda extranjera, las tarifas de transferencia ni ninguna otra tarifa de procesamiento en la que pueda incurrir. Su informe de ingresos mostrará el precio de venta (en la moneda local) y sus ingresos convertidos (en USD).
5.2 Cupones de instructor y enlaces de referencia a cursos

La plataforma de Udemy le permite generar códigos de cupón y enlaces de referencia para ofrecer determinados elementos de su Contenido remitido a estudiantes con descuento, al precio actual de Udemy o de forma gratuita, según lo permitido en los Servicios. Estos códigos de cupón y enlaces de referencia están sujetos a límites. No puede venderlos en sitios web de terceros ni ofrecerlos a cambio de una compensación. Encontrará información adicional y las restricciones que afectan a estos códigos de cupón y enlaces de referencia en nuestras políticas de Confianza y seguridad.

Si un estudiante aplica su código de cupón o enlace de referencia en el momento de pagar, su participación en los ingresos será el 97 % del Total neto menos las deducciones aplicables, como el reembolso al estudiante.
5.3 Recepción de los pagos

Para que le paguemos en el plazo debido, debe poseer una cuenta de PayPal, una cuenta de Payoneer o una cuenta bancaria estadounidense (solo para residentes en los EE. UU.) activa y mantenernos informados del correo electrónico correcto asociado a su cuenta. También debe proporcionar la información de identificación o documentación fiscal (como un formulario W-9 o W-8) que sea necesaria para abonarle los montos adeudados, y acepta que tenemos el derecho de retener los impuestos apropiados de sus pagos. Nos reservamos el derecho de retener los pagos o imponer otras sanciones si no recibimos la información de identificación o documentación fiscal apropiada por su parte. Entiende y acepta que usted es el responsable en última instancia de cualquier impuesto sobre sus ingresos.

En función del modelo de reparto de ingresos aplicable, el pago se efectuará en un plazo de 45 días desde el final del mes en el que (a) cobremos un curso o (b) tuviese lugar el consumo del curso pertinente.

Como instructor, tiene la responsabilidad de determinar si cumple o no los requisitos para que una empresa de EE. UU. le pague. Nos reservamos el derecho de no abonar pagos si se identifican fraudes, infracciones de los derechos de propiedad intelectual u otras infracciones de la ley.

Si no podemos liquidar los fondos en su cuenta de pago tras el periodo establecido por su estado, país u otra autoridad gubernamental en sus leyes de propiedad no reclamada, podemos procesar los fondos que se le deban de conformidad con nuestras obligaciones legales, incluido mediante el envío de esos fondos a la autoridad gubernamental correspondiente según lo exija la ley.
5.4 Reembolsos

Acepta que los estudiantes tienen el derecho de recibir un reembolso, tal y como se detalla en las Condiciones de uso. Los instructores no recibirán ningún ingreso de las transacciones a las que se haya concedido un reembolso en virtud de las Condiciones de uso.

Si un estudiante solicita un reembolso después de que hayamos abonado el pago del instructor correspondiente, nos reservamos el derecho de (1) deducir el importe del reembolso del siguiente pago que se envíe al instructor o de (2) exigir al instructor que devuelva los importes que se hayan reembolsado a los estudiantes por su Contenido remitido, en el caso de que no se tengan más pagos pendientes con el instructor o que los pagos no sean suficientes para cubrir los importes reembolsados.
6. Marcas registradas

Mientras sea un instructor con cursos publicados y sujeto a los requisitos que se presentan a continuación, puede utilizar nuestras marcas registradas donde se lo autoricemos.

Debe:

    utilizar únicamente las imágenes de nuestras marcas registradas que pongamos a su disposición, tal y como se detalle en las directrices que publiquemos;
    utilizar únicamente nuestras marcas registradas en relación con la promoción y la venta de su Contenido remitido disponible en Udemy o su participación en Udemy; y
    atenerse de inmediato si le solicitamos que suspenda su uso.

No debe:

    utilizar nuestras marcas registradas de una forma engañosa o denigrante;
    utilizar nuestras marcas registradas de una forma que implique que respaldamos, patrocinamos o aprobamos su Contenido remitido o sus servicios; o
    utilizar nuestras marcas registradas de una forma que infrinja la ley aplicable o en relación con un tema o un material obsceno, indecente o ilegal.

7. Eliminación de su cuenta

Podrá encontrar instrucciones para eliminar su cuenta de instructor aquí. Realizaremos todos los esfuerzos razonables desde el punto de vista comercial para abonar los pagos programados restantes que se le deban antes de eliminar su cuenta. Entiende que, si se han inscrito anteriormente estudiantes a su Contenido remitido, esos estudiantes podrán seguir accediendo a su nombre y a ese Contenido remitido después de que se elimine su cuenta. Si necesita ayuda o tiene dificultades para eliminar su cuenta, puede ponerse en contacto con nosotros a través de nuestro Centro de asistencia.
8. Otras condiciones legales
8.1 Actualización de estas Condiciones

De vez en cuando, puede que actualicemos estas Condiciones para clarificar nuestras prácticas o reflejar prácticas nuevas o diferentes (por ejemplo, cuando añadimos funciones nuevas), y Udemy se reserva el derecho, a su exclusivo criterio, de modificar o realizar cambios en estas Condiciones en cualquier momento. Si realizamos un cambio material, se lo notificaremos por uno de los medios principales: por ejemplo, enviándole un aviso por correo electrónico a la dirección especificada en su cuenta o mediante de la publicación del aviso a través de nuestros Servicios. A menos que se indique lo contrario, las modificaciones entrarán en vigor el día que se publiquen.

Si continúa utilizando los Servicios después de la entrada en vigor de los cambios, confirmará así que acepta dichos cambios. Las Condiciones revisadas sustituirán a todas las Condiciones indicadas anteriormente.
8.2 Traducciones

Toda versión de estas Condiciones en un idioma distinto del inglés se facilita para su comodidad. Por tanto, confirma y acepta que la versión en inglés prevalecerá si existieran discrepancias.
8.3 Relación entre las partes

Tanto usted como nosotros aceptamos que entre nosotros no existe una relación de empresa conjunta, asociación, empleo, contratista o agencia.
8.4 Supervivencia

Las siguientes secciones seguirán vigentes tras el vencimiento o la rescisión de estas Condiciones: secciones 2 (Licencia para Udemy), 3.3 (Relación con otros usuarios), 5.3 (Recepción de los pagos), 5.4 (Reembolsos), 7 (Eliminación de su cuenta) y 8 (Otras condiciones legales).
9. Cómo ponerse en contacto con nosotros

La mejor manera de ponerse en contacto con nosotros es a través del Equipo de asistencia. Estaremos encantados de que nos comunique sus preguntas, preocupaciones y comentarios sobre nuestros Servicios.
